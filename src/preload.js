const Graph = require('./graph.js');
const { Entities } = require('./entities.js');
const { Scripts } = require('./scripts.js');

window.addEventListener('DOMContentLoaded', () => {

  window.dispatchEvent(new CustomEvent("Log", {detail: {message: "System: " + process.platform + " (" + process.arch + ")"}}));
  window.dispatchEvent(new CustomEvent("Log", {detail: {message: "PID: " + process.pid}}));
  window.dispatchEvent(new CustomEvent("Log", {detail: {message: "PPID: " + process.ppid}}));
  window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Chrome: " + process.versions.chrome}}));
  window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Electron: " + process.versions.electron}}));
  window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Node: " + process.versions.node}}));
  window.dispatchEvent(new CustomEvent("Log", {detail: {message: "V8: " + process.versions.v8}}));

  setInterval(() => {
    document.querySelector("#cpu-usage").innerHTML = "CPU: " + process.getCPUUsage().percentCPUUsage.toString().slice(0,4) + "%";
    document.querySelector("#heap-size").innerHTML = "Heap: " + process.getHeapStatistics().totalHeapSize;
  }, 5000);

  /*var nodes_data =  [
    {"id": 0, "title": "@me", 'icon': "fab fa-twitter", 'icon-color': "#00acee"},
    {"id": 1, "title": "myaccount", 'icon': "fab fa-facebook", 'icon-color': "#4267B2"},
    {"id": 2, "title": "John Doe", 'icon': "fas fa-user", "force": 20, 'background-color': "#AAA", 'icon-color': "white", 'border-color': "#F00"},
    {"id": 3, "title": "https://google.com/", 'icon': "fas fa-server"},
    {"id": 4, "title": "8.8.8.8", 'icon': "fas fa-ethernet"},
    {"id": 5, 'icon': "fab fa-instagram", 'icon-color': 'instagram'},
    new entities.list[6]("Instagram")
  ]

  var links_data = [
    {"id": 0, "source": 2, "target": 0, "title": "account", "length": 50, "tip": "end"},
    {"id": 1, "source": 2, "target": 1, "title": "account", "length": 70, "tip": "end"},
    {"id": 2, "source": 4, "target": 3, "tip": "both"},
    {"id": 3, "source": 3, "target": 0, "length": 70},
    {"id": 4, "source": 3, "target": 5, "length": 50},
    {"id": 5, "source": 3, "target": 1, "length": 70}
  ]*/

  // window.dispatchEvent(new CustomEvent("Alert", {detail: {type: "error", message: "Whoaa, this seems very dangerous.", details: "But is it realy ?"}}));
  // window.dispatchEvent(new CustomEvent("Alert", {detail: {type: "warning", message: "This one looks pretty safe.", details: "And is"}}));
  // setTimeout(()=>{ window.dispatchEvent(new CustomEvent("Alert", {detail: {type: "Warning", message: "This is a warning"}})); }, 500);
  // setTimeout(()=>{ window.dispatchEvent(new CustomEvent("Alert", {detail: {type: "success", message: "It works !"}})); }, 1000);
  // setTimeout(()=>{ window.dispatchEvent(new CustomEvent("Alert", {detail: {message: "Normal message"}})); }, 1500);

  var entities = new Entities();
  var scripts = new Scripts();
  var graph = new Graph('#graph', [], []); //args: selector, nodes_data, links_data
  // setTimeout((graph)=>{ graph.add_nodes(nodes_data); }, 1000, graph);
  // setTimeout((graph)=>{ graph.add_links(links_data); }, 3000, graph);
  // setTimeout((graph)=>{ graph.remove_nodes([6]); }, 3000, graph);
  // setTimeout((graph)=>{ graph.set_nodes({"id": 0, "title": "@me", 'icon': "fab fa-twitter", 'icon-color': "#00acee"}); }, 3500, graph);
  // setTimeout((graph)=>{ graph.set_links(); }, 3500, graph);
  //
  // toast().warning('Hey!', 'There was a minor error!').show() //display with all parameters

  //window.dispatchEvent(new CustomEvent("Alert", {detail: {message: "Script started!"}}));
  window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Script started!"}}));
  //window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Script started!"}}));
  scripts.list[1].commands.test({URL: "http://cyprienisnard.eu5.net"}).then(results => {
    var nodes = graph.get_nodes();
    var last_node_index = nodes.length ? nodes.reduce((a,b)=>a.index>b.index?a:b).index : -1;
    var new_nodes = [];
    last_node_index++;
    var root_index = last_node_index;
    new_nodes.push(new entities.list[13](last_node_index, "http://cyprienisnard.eu5.net", "http://cyprienisnard.eu5.net"))

    var links = graph.get_links();
    var last_link_index = links.length ? links.reduce((a,b)=>a.id>b.id?a:b).id : -1;
    var new_links = [];
    results.forEach((result) => {
      entities.list.forEach((entity, i) => {
        if (entity.name === result?.type) {
          last_node_index++;
          new_nodes.push(new entity(last_node_index, result?.value, result?.value))
          last_link_index++;
          new_links.push({"id": last_link_index, "source": root_index, "target": last_node_index, "tip": "end", "length": 70})
        };
      });
    });

    graph.add_nodes(new_nodes);
    graph.add_links(new_links);
    window.dispatchEvent(new CustomEvent("Alert", {detail: {type: "success", message: "Script's done!"}}));
  });
});
