const d3 = require('d3');

module.exports = class Graph
{
    constructor(selector, nodes_data, links_data)
    {
        this.radius = 15;
        this.simulation = null;
        this.nodes_data = nodes_data;
        this.links_data = links_data;
        this.selector = selector;

        /*Main Canva*/

        this.canva = d3.select(this.selector)
          .append("svg")
          .attr("width", "100%")
          .attr("height", "100%")
          .style("cursor","move")
          .call(d3.zoom()
            .scaleExtent([0.1, 8])
            .on("zoom", (event) => {
              if (event.sourceEvent.target.id === "context-menu") this.close_context_menu();
              d3.select('.nodes').attr("transform", event.transform)
              d3.select('.links').attr("transform", event.transform)
            })
          );

        /*Custom style*/

        this.style = d3.select('head')
        .append('style')
          .html(`
                .instagram-gradient > path {
                  fill: url(#instagram-gradient);
                }
          `);

        /*Svg Defs*/
        this.defs = this.canva.append("defs")

        /*Arrows tips*/

        this.defs.append("marker")
          .attr("id", "arrow-end")
          .attr("viewBox", "0 0 10 10")
          .attr("refX", "25")
          .attr("refY", "5")
          .attr("markerUnits", "strokeWidth")
          .attr("markerWidth", "10")
          .attr("markerHeight", "10")
          .attr("orient", "auto")
          .append("path")
          .attr("d", "M 0 0 L 11 5 L 0 10 z")
          .style("fill", "#CCC");

        this.defs.append("marker")
          .attr("id", "arrow-start")
          .attr("viewBox", "0 0 10 10")
          .attr("refX", "25")
          .attr("refY", "5")
          .attr("markerUnits", "strokeWidth")
          .attr("markerWidth", "10")
          .attr("markerHeight", "10")
          .attr("orient", "auto-start-reverse")
          .append("path")
          .attr("d", "M 0 0 L 11 5 L 0 10 z")
          .style("fill", "#CCC");

        /*Instagram color gradient*/

        this.defs.append("radialGradient")
          .attr('id', 'instagram-gradient')
          .attr('cx', '30%')
          .attr('cy', '107%')
          .attr('r', '107%')
          .html('<stop offset="0%" style="stop-color:#fdf497;stop-opacity:1" /> <stop offset="5%" style="stop-color:#fdf497;stop-opacity:1" /> <stop offset="45%" style="stop-color:#fd5949;stop-opacity:1" /> <stop offset="60%" style="stop-color:#d6249f;stop-opacity:1" /> <stop offset="90%" style="stop-color:#285AEB;stop-opacity:1" />')

        this.redraw(this.selector); //draw the graph

        /*Context Menu*/

        this.open_context_menu = (event, canva) => {
            event.preventDefault();
            console.log("Open");
            console.log(event);
            window.addEventListener('keyup', this.close_context_menu, true);
            window.addEventListener('wheel', this.close_context_menu, true);
          	window.addEventListener('mousedown', this.close_context_menu, true);
          	window.addEventListener('focus', this.close_context_menu, true);
            canva.append("rect")
              .attr("width", "10%")
              .attr("height", "50%")
              .attr("id", "context-menu")
              .attr("fill", '#CCC')
              .attr("x", event.layerX)
              .attr("y", event.layerY);
            return false;
        };

        this.close_context_menu = (event) => {
          if (event && event.type === "mousedown" && event.target.id === "context-menu") return false;
          console.log('Close');
          console.log(event);
        	window.removeEventListener('keyup', this.close_context_menu, true);
        	window.removeEventListener('wheel', this.close_context_menu, true);
        	window.removeEventListener('mousedown', this.close_context_menu, true);
        	window.removeEventListener('focus', this.close_context_menu, true);
          d3.selectAll("#context-menu").remove();
        };

        this.close_delete_node = (event) => {
          if (event && event.type === "mousedown" && event.target.id === "delete_node") return false;
          window.removeEventListener('keyup', this.close_delete_node, true);
          window.removeEventListener('wheel', this.close_delete_node, true);
          window.removeEventListener('mousedown', this.close_delete_node, true);
          window.removeEventListener('focus', this.close_delete_node, true);
          d3.selectAll("#delete_node").remove();
          d3.selectAll("#delete_node_icon").remove();
        };
    }

    tick_actions() {

    //Select icons
    var nodes_icons = d3.selectAll('.node-icon').nodes();
    var nodes_titles = d3.selectAll('.node-title').nodes();
    var delete_node = d3.select('#delete_node').nodes();
    var delete_node_icon = d3.select('#delete_node_icon').nodes();

    d3.selectAll('.node').each(function(d, i, nodes) {
      //update circles positions each tick of the simulation
      d3.select(nodes[i])
         .attr("cx", function(d) { if (d) return d.x; })
         .attr("cy", function(d) { if (d) return d.y; });
      //update icons positions each tick of the simulation
      d3.select(nodes_icons[i])
        .attr("x", function(d) { return d3.select(nodes[i]).attr("cx") - 10 + "px" })
        .attr("y", function(d) { return d3.select(nodes[i]).attr("cy") - 10 + "px" });
      //update title positions each tick of the simulation
      d3.select(nodes_titles[i])
        .attr("x", function(d) { return d3.select(nodes[i]).attr("cx") + "px" })
        .attr("y", function(d) { return (parseFloat(d3.select(nodes[i]).attr("cy")) + 30) + "px" });

      if (delete_node[0]?.target === nodes[i]) {
        d3.select(delete_node[0])
          .attr("cx", function(d) { return d3.select(nodes[i]).attr("cx") + "px"})
          .attr("cy", function(d) { return d3.select(nodes[i]).attr("cy") - 25 + "px" });

        d3.select(delete_node_icon[0])
          .attr("x", function(d) { return d3.select(nodes[i]).attr("cx") - 4 + "px"})
          .attr("y", function(d) { return d3.select(nodes[i]).attr("cy") - 29 + "px" });
      }
     });

     var links_titles = d3.selectAll('.link-title').nodes();

      d3.selectAll('.link').each(function(d, i, nodes) { //Where link is the class name of link elements.
        d3.select(nodes[i])
          .attr("x1", function(d) { if (d) return d.source.x; })
          .attr("y1", function(d) { if (d) return d.source.y; })
          .attr("x2", function(d) { if (d) return d.target.x; })
          .attr("y2", function(d) { if (d) return d.target.y; });

        var rotation = (Math.atan2(0 - (parseFloat(d3.select(nodes[i]).attr("y2")) - parseFloat(d3.select(nodes[i]).attr("y1"))), 0 - (parseFloat(d3.select(nodes[i]).attr("x2")) - parseFloat(d3.select(nodes[i]).attr("x1"))))) * 180 / Math.PI

        d3.select(links_titles[i])
          .attr("x", function(d) { return (parseFloat(d3.select(nodes[i]).attr("x1")) + ((parseFloat(d3.select(nodes[i]).attr("x2")) - parseFloat(d3.select(nodes[i]).attr("x1"))) / 2)) + "px"; })
          .attr("y", function(d) { return (parseFloat(d3.select(nodes[i]).attr("y1")) + ((parseFloat(d3.select(nodes[i]).attr("y2")) - parseFloat(d3.select(nodes[i]).attr("y1"))) / 2)) + "px"; })
          .style("transform-origin", `${(parseFloat(d3.select(links_titles[i]).attr("x")))}px ${(parseFloat(d3.select(links_titles[i]).attr("y")))}px`)
          .style("transform", `rotateZ(${rotation}deg) ${(rotation > -90 && rotation < 90) ? "scale(1)" : "scale(-1)"}`)
      });
    }

    /*Delete previous graph if exist and redraw */

    redraw(selector) {

      this.canva.select(".links").remove();
      this.canva.select(".nodes").remove();

      /*Nodes links*/
      this.links = this.canva.append("g")
        .attr("class", "links")

      this.links.selectAll("line")
        .data(this.links_data)
        .enter()
        .append("line")
          .attr("stroke-width", 1)
          .attr("marker-end", (d) => { if(d["tip"] && (d["tip"] === "end" || d["tip"] === "both")) return "url(#arrow-end)" })
          .attr("marker-start", (d) => { if(d["tip"] && (d["tip"] === "start" || d["tip"] === "both")) return "url(#arrow-start)" })
          .attr("class", "link")
          .style("stroke", "#9996");

      this.links.selectAll("text")
        .data(this.links_data)
        .enter()
        .append("text")
          .attr("class", "link-title")
          .attr("pointer-events", "none")
          .attr("text-anchor", "middle")
          .attr("font-size", "0.8em")
          .attr("fill", "#999A")
          .text((d) => { return (d?.title) ? d.title : ""; })

      /*Nodes*/
      this.nodes = this.canva.append("g")
        .attr("class", "nodes")

      this.nodes.selectAll("circle")
      .data(() => {return this.nodes_data})
      .enter()
      .append("circle")
      .attr("r", this.radius)
      .attr("fill", (d) => { return (d["background-color"]) ? d["background-color"] : "white"; })
      .attr("stroke", (d) => { return (d["border-color"]) ? d["border-color"] : "black"; })
      .attr("class", "node")
      .attr("id", (d) => { return "node-" + d.id})
      .call(d3.drag()
      .on("start", (event, d) => {
        if (!event.active) this.simulation.alphaTarget(0.3).restart();
        d.fx = this.x;
        d.fy = this.y;
      })
      .on("drag", (event, d) => {
        d.fx = event.x;
        d.fy = event.y;
      })
      .on("end", (event, d) => {
        if (!event.active) this.simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
      }))
      .on('contextmenu', (event) => {
        this.open_context_menu(event, this.canva)
      })
      .on('click', (event) => {

        window.addEventListener('keyup', this.close_delete_node, true);
        window.addEventListener('wheel', this.close_delete_node, true);
        window.addEventListener('mousedown', this.close_delete_node, true);
        window.addEventListener('focus', this.close_delete_node, true);

        if (d3.selectAll("#delete_node,#delete_node_icon").nodes().length)
          d3.selectAll("#delete_node,#delete_node_icon").remove()

        var delete_node_group = this.nodes.append("g")
        var delete_node = delete_node_group.append('circle')
            .attr("r", "5px")
            .attr("r", "5px")
            .attr("id", "delete_node")
            .attr("fill", '#CCC')
            .attr("cx", d3.select(event.target).attr('cx'))
            .attr("cy", parseFloat(d3.select(event.target).attr('cy')) - 29)
            .on('click', (event) => {
              this.remove_nodes(this.nodes_data.filter((n) => {return ("node-" + n.id) === event.target.target.id})[0].id);
              d3.selectAll("#delete_node,#delete_node_icon").remove();

              window.removeEventListener('keyup', this.close_delete_node, true);
              window.removeEventListener('wheel', this.close_delete_node, true);
              window.removeEventListener('mousedown', this.close_delete_node, true);
              window.removeEventListener('focus', this.close_delete_node, true);
            });
        delete_node.nodes()[0].target = event.target;

        delete_node_group.append("svg")
        .attr("class", "fas fa-times")
        .attr("id", "delete_node_icon")
        .attr("height", "8px")
        .attr("width", "8px")
        .attr("x", d3.select(event.target).attr('cx'))
        .attr("y", parseFloat(d3.select(event.target).attr('cy')) - 29)
        .attr('pointer-events', 'none')
      });

      this.nodes.selectAll("svg")
        .data(this.nodes_data)
        .enter()
        .append("svg")
        .attr("class", (d) => { return "node-icon " + ((d?.icon) ? (d["icon-color"] === "instagram") ? "instagram-gradient " + d.icon : d.icon : "fas fa-question-circle") })
        .attr("height", "20px")
        .attr("width", "20px")
        .attr('pointer-events', 'none')
        .style('color', (d) => { return (d["icon-color"]) ? (d["icon-color"] === "instagram") ? "" : d["icon-color"] : "black"; })

      this.nodes.selectAll("text")
        .data(this.nodes_data)
        .enter()
        .append("text")
        .attr("class", "node-title")
        .attr("pointer-events", "none")
        .attr("text-anchor", "middle")
        .attr("font-size", "0.8em")
        .text((d) => { return (d?.title) ? d.title : "Entity #" + d.id; })

      this.nodes
        .on("mouseover", () => {
          this.canva.style("cursor","pointer");
        })
      /*.on("mousedown", function(d) {
          d3.event.stopPropagation();
          focus_node = d;
          set_focus(d)
          if (highlight_node === null) set_highlight(d)
        })*/
        .on("mouseout", () => {
          this.canva.style("cursor","move");
        });

        /*Remove previous simulation if exist*/
        delete this.simulation;

        /*Init simulation*/
        this.simulation = d3.forceSimulation()
          .nodes(this.nodes_data)
          .force("charge_force", d3.forceManyBody().strength(function(d) { return -10 - (d?.force || 0); }))
          .force("center_force", d3.forceCenter(document.querySelector(selector).clientWidth / 2, document.querySelector(selector).clientHeight / 2))
          .force("links", d3.forceLink(this.links_data).id(function(d) { return d.id; }).distance(function(d) {return (d?.title) ? (d.title.length * 12) + 50 + (d?.length || 0) : 90 + (d?.length || 0); }))
          .on("tick", this.tick_actions );
    }

    /*Public functions*/

    get_nodes() {
      return this.nodes_data;
    }

    get_links() {
      return this.links_data;
    }

    set_nodes(nodes) {
      nodes = nodes ? Array.isArray(nodes) ? nodes : [nodes] : [];
      this.nodes_data = nodes;
      this.redraw(this.selector);
    }

    set_links(links) {
      links = links ? Array.isArray(links) ? links : [links] : [];
      this.links_data = links;
      this.redraw(this.selector);
    }

    add_nodes(nodes) {
      nodes = nodes ? Array.isArray(nodes) ? nodes : [nodes] : [];
      nodes.map(node => {node.x = (document.querySelector(this.selector).clientWidth / 2); node.y = (document.querySelector(this.selector).clientHeight / 2); return node;} )
      this.nodes_data = this.nodes_data.concat(nodes);
      this.redraw(this.selector);
    };

    remove_nodes(ids) {
      ids = Array.isArray(ids) ? ids : [ids];
      this.nodes_data = this.nodes_data.filter(node => !ids.includes(node.id));
      this.links_data = this.links_data.filter(link => !ids.includes(link?.source?.id) && !ids.includes(link?.target?.id));
      this.redraw(this.selector);
    };

    add_links(links) {
      links = links ? Array.isArray(links) ? links : [links] : [];
      this.links_data = this.links_data.concat(links);
      this.redraw(this.selector);
    };

    remove_links(ids) {
      ids = Array.isArray(ids) ? ids : [ids];
      this.links_data = this.links_data.filter(link => !ids.includes(link.id));
      this.redraw(this.selector);
    };
}

// module.exports = {
//     Graph
// };
