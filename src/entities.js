const fs = require('fs');

class Entities
{
  constructor(dir)
  {
    this.dir = dir || './entities';
    if (!fs.existsSync(this.dir)) fs.mkdirSync(this.dir); //Check if the script directory exist

    this.list = fs.readdirSync(this.dir, { withFileTypes: true }) //List all inside script dir
      .filter(dirent => { if(dirent.name.match(/^.*(.json)$/g).length) return fs.lstatSync(this.dir+"/"+dirent.name).isFile(); else return false; }) //Keep only the subdirs
      .map((dirent, id) => { //Parse the script.json file
        try {
          var parsed_json = JSON.parse(fs.readFileSync((this.dir+"/"+dirent.name), 'utf-8'));
          var name = dirent.name.replace(/(.json)|([^A-Za-z0-9_])/g, "");
          name = name[0].toUpperCase() + name.slice(1);

          if (name) {
            var serialized_class = new Function(`
              return class ${name} {
                static id = ${id};
                static properties = ${JSON.stringify(parsed_json?.properties)};
                constructor(id, title, value, force, icon, background_color, icon_color, border_color)
                {
                  this.id = id;
                  this.title = title;
                  this.value = value;
                  this.force = force;
                  this["icon"] = icon || ${JSON.stringify(parsed_json?.icon)};
                  this["background-color"] = background_color || ${JSON.stringify(parsed_json?.background_color)};
                  this["icon-color"] = icon_color || ${JSON.stringify(parsed_json?.icon_color)};
                  this["border-color"] = border_color || ${JSON.stringify(parsed_json?.border_color)};
                }
              }
            `)();
            return serialized_class;
          }

          /*var jsonData = JSON.parse(fs.readFileSync((this.dir+"/"+dirent.name), 'utf-8'))
          jsonData.name = dirent.name.replace(/(.json)|([^A-Za-z0-9_])/g, "");
          jsonData.name = jsonData.name[0].toUpperCase() + jsonData.name.slice(1);
          jsonData.id = id;
          return (({ id, name, icon, background_color, icon_color, border_color, force, properties }) => ({ id, name, icon, background_color, icon_color, border_color, force, properties }))(jsonData);*/
        } catch (e) {
          console.log("Error parsing script " + dirent.name + ". Traceback: " + e);
        }
      }).filter(script => script)
  }
}

module.exports = {
    Entities
};
