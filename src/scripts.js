const hasbin = require('hasbin'); //check if program is available (in path, local, etc)
const { PythonShell } = require('python-shell');
const fs = require('fs');
const exec = require('child_process').exec;

class Scripts
{
    constructor(dir)
    {

      this.python_available = this.python_check();
      this.wsl_check().then(value => {this.wsl_available = value;}) /*@Todo check async !*/

      this.dir = dir || './scripts';
      if (!fs.existsSync(this.dir)) fs.mkdirSync(this.dir); //Check if the script directory exist

      this.list = fs.readdirSync(this.dir, { withFileTypes: true }) //List all inside script dir
        .filter(dirent => fs.lstatSync(this.dir+"/"+dirent.name).isDirectory()) //Keep only the subdirs
        .filter(dirent => fs.existsSync((this.dir+"/"+dirent.name)+"/script.json")) //Keep only the ones containing a script.json file
        .map((dirent, id) => { //Parse the script.json file
          try {
            var jsonData = JSON.parse(fs.readFileSync((this.dir+"/"+dirent.name)+"/script.json", 'utf-8'))
            jsonData.path = this.dir+"/"+dirent.name;
            jsonData.id = id;
            if (!Array.isArray(jsonData.parse)) jsonData.parse = [jsonData.parse];
            jsonData.parse.filter((rule) => (typeof (rule)?.type === 'string') && (typeof (rule)?.regex === 'string'))
            return (({ id, name, version, path, description, type, main, commands, tags, license, author, parse }) => ({ id, name, version, path, description, type, main, commands, tags, license, author, parse }))(jsonData);
          } catch (e) {
            console.log("Error parsing script " + dirent.name + ". Traceback: " + e);
          }
        }).filter(script => script) //Remove the scripts that failed parsing
        .map(script => { //Create the commands to run the scripts
            switch (script.type) { //Check the script type & platform (win32, darwin, linux)
              case 'python':
                console.log("Not implemented yet !");
                break;
              case 'elf':
                switch (process.platform) {
                  case "win32":
                    script.launcher = "wsl"
                    break;
                  default:
                    console.log(`The script ${script.type} cannot be executed on the platform ${process.platform} yet !`);
                }
                break;
              case "java":
                  script.launcher = "java -jar"
                break;
              default:
                console.log(`The script type ${script.type} is not known !`);
            }

            if (script.launcher != null)
            {
              //(\$[A-Z_]*)
              script.commands = Object.fromEntries(Object.entries(script.commands)
              .map(([name, command]) => {
                return [name, async (args) => {
                  if (args && typeof args === "object" && Object.keys(args).length)
                  {
                    Object.entries(args).forEach((k) => {
                      command = command.replace('$'+k[0].toUpperCase(), k[1])
                    });
                  }
                  return new Promise((resolve, reject) => {
                    exec(`${script.launcher} ${script.main} ${command}`.trimStart().trimEnd(),
                      {cwd: `${script.path}`},
                      (error, stdout, stderr) => {
                        if (error) reject({error: error, stderr: stderr});
                        else if (script.parse.length) {
                          var streams = [stdout, stderr].filter((stream) => stream);
                          var results = [];
                          streams.forEach((stream) => {
                            script.parse.forEach((rule) => {
                              results = results.concat(stream.match(new RegExp(rule.regex, 'g'))?.filter((matched) => matched).map((matched) => {return {type: rule.type, value: matched}}));
                            });
                          });
                          resolve(results);
                        } else {
                          resolve([stdout, stderr].filter((stream) => stream));
                        }
                      });
                  });
                }];
              }));
            } else script.commands = []

            return script;
        })
    }

    python_check() {
      // Check if a python 3 is available
      if ((hasbin.sync('python') || hasbin.sync('python3')) && (hasbin.sync('pip') || hasbin.sync('pip3'))) {
        PythonShell.runString('import sys\nprint(sys.version_info[0] == 3)', null, function (err, results) {
          if (err) throw err;
          else if (!(JSON.parse(results[0].toLowerCase()))) {
            //window.dispatchEvent(new CustomEvent("Alert", {detail: {type: "error", message: "Python 3 not found in path."}}));
            window.dispatchEvent(new CustomEvent("Log", {detail: {type: "error", message: "Python 3 not found in path."}}));
            return false;
          }
          else {
            window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Python 3 available."}}));
            PythonShell.runString('import subprocess\nimport sys\nsubprocess.check_call(["pipreqs", "--version"])', null, function (err, results) {
              if (err?.traceback.match(/(ModuleNotFoundError)/)) {
                PythonShell.runString('import subprocess\nimport sys\nsubprocess.check_call([sys.executable, "-m", "pip", "install", "pipreqs"])', null, function (err, results) {
                  if (err) throw err;
                  else window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Package pipreqs has been installed."}}));
                  return true;
                });
              }
              else if (err) console.log(err);
              else window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Package pipreqs already installed."}}));
              return true;
            });

            PythonShell.runString('import subprocess\nimport sys\nsubprocess.check_call(["pip-compile", "--version"])', null, function (err, results) {
              if (err) {
                console.log(err);
                PythonShell.runString('import subprocess\nimport sys\nsubprocess.check_call([sys.executable, "-m", "pip", "install", "pip-tools"])', null, function (err, results) {
                  if (err) throw err;
                  else window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Package pip-tools has been installed."}}));
                  return true;
                });
              } else window.dispatchEvent(new CustomEvent("Log", {detail: {message: "Package pip-tools already installed."}}));
              return true;
            });

          }
        });
      } else {
        //window.dispatchEvent(new CustomEvent("Alert", {detail: {type: "error", message: "Python or Pip not found in path."}}));
        window.dispatchEvent(new CustomEvent("Log", {detail: {type: "error", message: "Python or Pip not found in path."}}));
        return false;
      }
    }

    async wsl_check() {
      if (hasbin.sync('wsl')) {
        return await new Promise((resolve, reject) => {
        exec("wsl --status",
          (error, stdout, stderr) => {
            if (stdout.toString().replaceAll('\0', "").length > 0) {
              window.dispatchEvent(new CustomEvent("Log", {detail: {message: "WSL available."}}));
              resolve(true);
            } else {
              window.dispatchEvent(new CustomEvent("Log", {detail: {type: "error", message: "WSL not found in path."}}));
              resolve(false);
            }
          });
        })
      } else {
        window.dispatchEvent(new CustomEvent("Log", {detail: {type: "error", message: "WSL not found in path."}}));
      }
    }
}

module.exports = {
  Scripts
};
