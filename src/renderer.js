import 'bootstrap';
import Split from 'split.js';
import './scss/app.scss';
import "@fortawesome/fontawesome-free/js/all";

import './js/index.js';
import './js/toasts.js';
import './js/terminal.js';

Split(['.split-1', '.split-2'], {
    sizes: [15, 85],
    gutterSize: 4,
})

var console_gutter = Split(['.split-3', '.split-4'], {
    direction: 'vertical',
    gutterSize: 4,
})

$(console_gutter.pairs[0].gutter).css("display", "none");
$( ".split-4" ).first().css("height", "");
$( ".split-4" ).first().css("max-height", "300px");
