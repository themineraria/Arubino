window.addEventListener("Log", function(e) {
  var time = new Date().toISOString().slice(11, 19);
  var text_color = (e.detail?.type?.toLowerCase() === "error") ? "text-danger" : (e.detail?.type?.toLowerCase() === "warning") ? "text-warning" : (e.detail?.type?.toLowerCase() === "success") ? "text-success" : "text-muted";
  $("#console-content").prepend(`<p class="` + text_color + ` p-0 m-0">[` + time + `] ` + e.detail.message + `</p>`)
})
