import { Toast, Collapse } from 'bootstrap/dist/js/bootstrap.bundle.js';

window.addEventListener("Alert", function(e) {
   var bg_color = (e.detail?.type?.toLowerCase() === "error") ? "bg-danger" : (e.detail?.type?.toLowerCase() === "warning") ? "bg-warning" : (e.detail?.type?.toLowerCase() === "success") ? "bg-success" : "bg-light";
   var header_text_color = (["error", "warning", "success"].includes(e.detail?.type?.toLowerCase())) ? "text-white" : "";
   var header_text = (e.detail?.title || ((e.detail?.type?.toLowerCase() === "error") ? "Error" : (e.detail?.type?.toLowerCase() === "warning") ? "Warning" : (e.detail?.type?.toLowerCase() === "success") ? "Success" : "Message"));
   var btn_close_color = (["error", "warning", "success"].includes(e.detail?.type?.toLowerCase())) ? "btn-close-white" : "";
   var body_text_color = (e.detail?.type?.toLowerCase() === "error") ? "text-danger" : (e.detail?.type?.toLowerCase() === "warning") ? "text-warning" : (e.detail?.type?.toLowerCase() === "success") ? "text-success" : "";
   var auto_hide = (e.detail?.autohide || (e.detail?.details ? "false" : "true"));
   var hide_delay = (e.detail?.hidedelay || "5000");
   var details_btn = (e.detail?.details ? ` <a class="collapse-btn" href="#">details ...</a>` : "" );
   var details = (e.detail?.details ? `<div class="collapse"><div class="card card-body text-muted">`+e.detail?.details+`</div></div>` : "" );
   var icon = (e.detail?.type?.toLowerCase() === "error") ? `<i class="fas fa-exclamation-circle" style="margin-right: 4px;"></i>` : (e.detail?.type?.toLowerCase() === "warning") ? `<i class="fas fa-exclamation-triangle" style="margin-right: 4px;"></i>` : (e.detail?.type?.toLowerCase() === "success") ? `<i class="fas fa-check-circle" style="margin-right: 4px;"></i>` : "";
   var Alert = $( `<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-autohide="` + auto_hide + `" data-bs-delay="` + hide_delay + `">
      <div class="toast-header ` + bg_color + ` ` + header_text_color + `">
        ` + icon + `
        <strong class="me-auto">` + header_text + `</strong>
        <small class="countdown ` + header_text_color + `">just now</small>
        <button type="button" class="btn-close ` + btn_close_color + `" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
      <div class="toast-body ` + body_text_color + `">
        <p>
        ` + (e.detail?.message || "") + details_btn +`
        </p>
        ` + details + `
      </div>
    </div>` ).appendTo("#toast-container");
    let bsAlert = new Toast(Alert[0]);
    bsAlert.show();
    var seconds = 0;
    var counter = setInterval(() => {
      seconds++;
      $(Alert).find(".countdown").text(seconds + " seconds ago");
    }, 1000);
    Alert[0].addEventListener('hidden.bs.toast', function () {
      $(Alert[0]).remove();
      clearInterval(counter);
      counter = 0;
    })

    if(details)
    {
      var Details = $(Alert).find(".collapse")
      var bsCollapse = new Collapse(Details[0], {
        toggle: false
      })
      $(Alert).find(".collapse-btn").on('click', () => {bsCollapse.toggle()})
    }
 });
