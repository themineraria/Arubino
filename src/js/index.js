$('#console-hidder').on( "click", function() {
  if ($( this ).children('svg').hasClass( "clicked" ))
  {
    $( this ).children('svg').removeClass("clicked");
    $( this ).children('svg').css("transform", "rotate(0deg)");
    $( ".split-4" ).first().css("height", "");
    $( ".split-4" ).first().prev().css("display", "none");
    $("#console-content").css("display", "none");
  } else {
    $( this ).children('svg').addClass("clicked");
    $( this ).children('svg').css("transform", "rotate(180deg)");
    $( ".split-4" ).first().css("height", "");
    $( ".split-4" ).first().prev().css("display", "");
    $("#console-content").css("display", "block");
  }

});
